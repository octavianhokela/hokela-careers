import Vue from 'vue'
import VueRouter from 'vue-router'
import CareerPage from '../components/CareerPage'
import Career from '../views/Career.vue' 

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'careers',
    component: Career
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/career/:id/',
    name: 'career',
    component: CareerPage
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
